# Change these
server '192.168.0.155', port: 22, roles: [:web, :app, :db], primary: true

set :repo_url,        'git@bitbucket.org:A1eksandr/angular_example.git'
set :application,     'angular_example'
set :user,            'developer'
set :puma_threads,    [4, 16]
set :puma_workers,    0

# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, false  # Change to true if using ActiveRecord

## Defaults:
# set :scm,           :git
# set :branch,        :master
# set :format,        :pretty
# set :log_level,     :debug
# set :keep_releases, 5

## Linked Files & Directories (Default None):
# set :linked_files, %w{config/database.yml}
# set :linked_dirs,  %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma
# config valid only for current version of Capistrano
# lock '3.4.0'

# set :application, 'angular_example'
# set :repo_url, 'git@bitbucket.org:A1eksandr/angular_example.git' # change this to your git server

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# set :use_sudo, false
# set :bundle_binstubs, nil
# set :linked_files, fetch(:linked_files, []).push('config/database.yml')
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# after 'deploy:publishing', 'deploy:restart'

# namespace :deploy do
#   task :restart do
#     invoke 'unicorn:stop'
#     invoke 'unicorn:reload'
#   end

#   task :stop do
#     invoke 'unicorn:stop'
#   end
# end

# old
# set :ssh_options, {
#   forward_agent: true
# }

# set :application, 'angular_example'
# set :deploy_to,   "/home/def/www/#{fetch(:application)}"

# set :bundle_without,  [:development, :test]

# set :rvm_ruby_version, "2.2.1@global"
# set :rake,            "rvm use #{fetch(:rvm_ruby_version)} do bundle exec rake"
# set :bundle_cmd,      "rvm use #{fetch(:rvm_ruby_version)} do bundle"

# set :repo_url,        'git@bitbucket.org:A1eksandr/angular_example.git'

# set :use_sudo,        false

# after "deploy", "deploy:cleanup"

# set :linked_files, %w{config/database.yml}
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# namespace :deploy do

#   desc 'Restart application'
#   task :restart do
#     on roles(:app), in: :sequence, wait: 5 do
#       execute :touch, release_path.join('tmp/restart.txt')
#     end
#   end

#   after :publishing, :restart

#   desc "Make sure local git is in sync with remote."
#   task :check_revision do
#     on roles(:web) do
#       unless `git rev-parse HEAD` == `git rev-parse origin/master`
#         puts "WARNING: HEAD is not the same as origin/master"
#         puts "Run `git push` to sync changes."
#         exit
#       end
#     end
#   end

#   before "deploy", "deploy:check_revision"
# end
